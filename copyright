Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2003 Simon Kelley <simon@thekelleys.org.uk>
License: GPL-2.0-only

Files:
 images/*.bin
 images.usb/*.bin
Copyright: 2004, Atmel Corporation
Source: http://www.thekelleys.org.uk/atmel/atmel-firmware-1.2.tar.gz
Comment:
 The bin files in the images were generated from header files
 included with the 2.1.1 release of the "Atmel drivers", released
 by Atmel corp in December 2002 and subsequent modifications,
 downloaded from atmelwlandriver.sourceforge.net 
 .
 The copyright on these files was modified (by Atmel corp)
 in November 2004 to the form shown below.
License: Atmel
 Copyright (c) 2004 Atmel Corporation. All Rights Reserved.        
 Redistribution and use of the microcode software ( Firmware ) is        
 permitted provided that the following conditions are met:        
 .
         1. Firmware is redistributed in object code only;        
         2. Any reproduction of Firmware must contain the above        
            copyright notice, this list of conditions and the below   
            disclaimer in the documentation and/or other materials        
            provided with the distribution; and        
         3. The name of Atmel Corporation may not be used to endorse        
            or promote products derived from this Firmware without specific        
            prior written consent.        
 .
 DISCLAIMER: ATMEL PROVIDES THIS FIRMWARE  "AS IS" WITH NO WARRANTIES OR        
 INDEMNITIES WHATSOEVER. ATMEL EXPRESSLY DISCLAIMS ANY EXPRESS, STATUTORY        
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED        
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND        
 NON-INFRINGEMENT. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,        
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES        
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR        
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)        
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,        
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN        
 ANY WAY OUT OF THE USE OF THIS FIRMWARE, EVEN IF ADVISED OF THE        
 POSSIBILITY OF SUCH DAMAGE. USER ACKNOWLEDGES AND AGREES THAT THE        
 PURCHASE OR USE OF THE FIRMWARE WILL NOT CREATE OR GIVE GROUNDS FOR A        
 LICENSE BY IMPLICATION, ESTOPPEL, OR OTHERWISE IN ANY INTELLECTUAL        
 PROPERTY RIGHTS (PATENT, COPYRIGHT, TRADE SECRET, MASK WORK, OR OTHER        
 PROPRIETARY RIGHT) EMBODIED IN ANY OTHER ATMEL HARDWARE OR FIRMWARE        
 EITHER SOLELY OR IN COMBINATION WITH THE FIRMWARE.        

License: GPL-2.0-only
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
