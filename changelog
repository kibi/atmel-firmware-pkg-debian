atmel-firmware (1.3-7) UNRELEASED; urgency=medium

  * Move source and binary from non-free/net to non-free-firmware/kernel
    following the 2022 General Resolution about non-free firmware:
     - replace non-free with non-free-firmware;
     - replace /net with /kernel to match how the packages are getting
       published (via overrides on the ftp-master side).

 -- Cyril Brulebois <kibi@debian.org>  Mon, 16 Jan 2023 23:29:53 +0000

atmel-firmware (1.3-6) unstable; urgency=medium

  * Change maintainer to Ryan Finnie, with Simon Kelley's approval.
    Thank you Simon!
  * copyright: Fix Atmel license text formatting.
  * Add Suggests: perl (full perl needed for atmel_fwl).
  * Remove Conflicts: atmelwlandriver-tools.
  * Adjust package description, remove "2.6.x".
  * Remove postinst hook, all hook functions are obsolete.
  * Remove trailing whitespace in files for lintian --pedantic.
  * Add Homepage, Vcs-Browser, Vcs-Git.

 -- Ryan Finnie <ryan@finnie.org>  Fri, 14 Jan 2022 17:33:02 -0800

atmel-firmware (1.3-5) unstable; urgency=medium

  * Maintainer source-only upload.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sun, 9 Jan 2022 21:57:32 +0000

atmel-firmware (1.3-4.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Convert package to debhelper (Closes: #999185)
  * Convert debian/copyright to DEP-5
  * Clean lintian (except for pedantic whitespace)

 -- Ryan Finnie <ryan@finnie.org>  Tue, 28 Dec 2021 22:06:04 -0800

atmel-firmware (1.3-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Restore /etc/pcmcia/atmel.conf being a conffile. (Closes: #988715)

 -- Adrian Bunk <bunk@debian.org>  Wed, 16 Jun 2021 15:17:00 +0300

atmel-firmware (1.3-4) unstable; urgency=low

   * remove problematic dependencies. (closes: #492271)

 -- Simon Kelley <simon@thekelleys.org.uk>  Thu, 24 Jul 2008 20:50:12 +0000

atmel-firmware (1.3-3) unstable; urgency=low

   * provide DEBIAN/md5sums.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sat, 18 Aug 2007 15:21:22 +0000

atmel-firmware (1.3-2) unstable; urgency=low

   * Depend on hotplug OR udev. (closes: #332889)
   * Moved firmware to /lib/firmware.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sun, 9 Oct 2005 20:11:22 +0000

atmel-firmware (1.3-1) unstable; urgency=low

   * New upstream. (closes: #305199)

 -- Simon Kelley <simon@thekelleys.org.uk>  Sat, 7 May 2005 21:10:32 +0000

atmel-firmware (1.2-2) unstable; urgency=low

   * Fixed Debian copyright file - I missed that in 1.2-1.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sun, 16 Jan 2005 17:20:12 +0000

atmel-firmware (1.2-1) unstable; urgency=low

   * New upstream.
   * Added LG LW2100N to atmel.conf
   * Updated firmware license to Atmel's latest version.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sun, 09 Jan 2005 21:25:12 +0000

atmel-firmware (1.1-1) unstable; urgency=low

   * New upstream, adds another USB image.

 -- Simon Kelley <simon@thekelleys.org.uk>  Mon, 30 Aug 2004 19:55:12 +0000

atmel-firmware (1.0-3) unstable; urgency=low

   * Recoded atmel_fwl in perl and changed to arch all. (closes: #262214)

 -- Simon Kelley <simon@thekelleys.org.uk>  Sat, 31 July 2004 13:08:32 +0000

atmel-firmware (1.0-2) unstable; urgency=low

   * Bring copyrights into compliance with Debian policy. (closes: #259026)

 -- Simon Kelley <simon@thekelleys.org.uk>  Mon, 12 July 2004 21:15:12 +0000

atmel-firmware (1.0-1) unstable; urgency=low

   * Update license and move into non-free section.

 -- Simon Kelley <simon@thekelleys.org.uk>  Thurs, 13 May 2004 17:14:32 +0000

atmel-firmware (0.7-1) unstable; urgency=low

   * Fix SMC 2632 entries in atmel.conf
   * Make /etc/pcmcia/atmel.conf a conffile.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sun, 01 Feb 2004 10:24:52 +0000

atmel-firmware (0.6-1) unstable; urgency=low

   * Remove atmel_at76c504-wpa.bin which just a copy of atmel_at76c504.bin
   * Remove duplicate 3com 3CRWE62092B entry from atmel.conf.

 -- Simon Kelley <simon@thekelleys.org.uk>  Wed, 28 Jan 2004 10:24:52 +0000

atmel-firmware (0.5-1) unstable; urgency=low

   * Added Wavebuddy to atmel.conf & doc improvements.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sun, 25 Jan 2004 20:04:12 +0000

atmel-firmware (0.4-1) unstable; urgency=low

   * Renamed USB firmware to match PCMCIA.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sat, 17 Jan 2004 10:01:15 +0000

atmel-firmware (0.3-1) unstable; urgency=low

   * Added atmel.conf PCMCIA configuration.
   * Restart pcmcia in postinst.
   * Added USB firmware.
   * Added conflict with atmelwlandriver-tools

 -- Simon Kelley <simon@thekelleys.org.uk>  Wed, 14 Jan 2004 22:11:05 +0000

atmel-firmware (0.2-1) unstable; urgency=low

   * Initial release.

 -- Simon Kelley <simon@thekelleys.org.uk>  Sat, 27 Dec 2003 20:21:15 +0000
